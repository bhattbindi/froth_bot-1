package com.robotico.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.robotico.lib.CustomLoadableComponent;


public abstract class BaseObjectPage<T extends CustomLoadableComponent<T>> extends CustomLoadableComponent<T> {

	  private static Logger logger = LogManager.getLogger(BaseObjectPage.class);
	    private WebDriver driver;

	    public BaseObjectPage(RemoteWebDriver driver) {
	        this.driver = driver;
	    }

	    public T openPage(Class<T> clazz, String pageName) {
	        logger.info("Title = {}", driver.getTitle() + clazz.getName());
	        T page = null;
	        try {
	            page = PageFactory.initElements(driver, clazz);
	            page = page.get(pageName);
	        } catch (Exception e) {
	            logger.error(e);
	        }
	        return page;

	    }

	    public abstract String getPageUrl();

	    public void open(String url) {
	        driver.get(url);
	    }


}