package com.robotico.lib;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class ReadHTML {
	public static void main(String[] args) {
		try {
			File input = new File(
					"E:\\WORKSPACES\\ROBOTICO_WORKSPACE\\tenant\\extentreport20210127022229\\robotico_20210127022229.html");
			Document doc = Jsoup.parse(input, "UTF-8", "");
			String title = doc.title();
			System.out.println(title);

			Elements SCENARIO_no = doc.select("li.test-item div.detail-head div.info h5");
			System.out.println(SCENARIO_no.size());

			for (int i = 0; i < SCENARIO_no.size(); i++) {
				Elements SCENARIO = doc.select("li.test-item:nth-child(" + (i + 1) + ") div.detail-head div.info h5");
				System.out.println("------SCENARIO" + (i + 1) + "---------");
				System.out.println(SCENARIO.text());

				Elements testcasesBasedOnScenareio = doc.select(
						"li.test-item:nth-child(" + (i + 1) + ") div.test-contents div.card div.card-title div.node");
				for (int j = 0; j < testcasesBasedOnScenareio.size(); j++) {
					System.out.println("------TESTCASE" + (j + 1) + "---------");
					System.out.println(testcasesBasedOnScenareio.get(j).text());

					Elements steps_no = doc
							.select("li.test-item:nth-child(" + (i + 1) + ") div.test-contents div.card");
					System.out.println("NO OF STEPS IN TESTCASE " + (j + 1) + ":" + steps_no.size());

					Elements steps = doc.select("li.test-item:nth-child(" + (i + 1)
							+ ") div.test-contents div.card:nth-child(" + (j + 1)
							+ ") > div.collapse > div.card-body > table > tbody > tr.event-row > td:nth-child(3)");

					Elements pass_failsteps = doc.select("li.test-item:nth-child(" + (i + 1)
							+ ") div.test-contents div.card:nth-child(" + (j + 1)
							+ ") > div.collapse > div.card-body > table > tbody > tr.event-row > td:nth-child(1) >i");

					for (int l = 0; l < steps.size(); l++) {
						System.out.println("Step " + (l + 1) + " : " + steps.get(l).text() + " : "
								+ (pass_failsteps.get(l).attr("class").contains("pass") ? "pass" : "fail"));
					}

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
