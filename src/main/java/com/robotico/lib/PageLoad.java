package com.robotico.lib;

import static com.robotico.listener.ExtentTestNGITestListener.getTest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.robotico.base.DriverFactory;
import com.robotico.listener.ExtentTestNGITestListener;
import com.robotico.pageprop.HarUtil.HarUtil;

import net.lightbody.bmp.core.har.Har;

public class PageLoad extends DriverFactory {

	public static boolean myElementIsClickable(WebDriver driver, By by) {
		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(by));
		} catch (WebDriverException ex) {
			return false;
		}
		return true;
	}

	public static void captureNetwork(String pageName) throws IOException {
		HarUtil hu = new HarUtil();
		Har har = DriverFactory.getProxy().getHar();
		String harFileName = MainUtil.sdfyyyyMMddHHmmss.format(new Date()) + "_" + pageName + ".har";
		String harDir = MainUtil.file_separator +"home"+ MainUtil.file_separator +"testopsadmin"+ MainUtil.file_separator +"automation"+ MainUtil.file_separator +"harfiles" + MainUtil.file_separator + MainUtil.vm_no + MainUtil.file_separator +ExtentTestNGITestListener.executionId;
		File harDirPath = new File(harDir);
		if (!harDirPath.exists())
			harDirPath.mkdirs();

		  File outputPath=new File(harDir + MainUtil.file_separator + harFileName);
		  System.out.println(outputPath.getPath());
		FileOutputStream fos = new FileOutputStream(outputPath);
		har.writeTo(fos);
		/*
		 * File fileLocation = new File(".//" + pageName + ".json");
		 * har.writeTo(fileLocation);
		 */
		hu.harParser(DriverFactory.getProxy().getHar());
		String pageData[][] = { { pageName,
				"<span style='color:" + ((hu.totalPageLoadTime_decimal < 10) ? "green" : "red") + ";font-weight:bold'>"
						+ hu.totalPageLoadTime_decimal + " sec" + "</span>",
				"5 sec", hu.totalPageSize_decimal + " KB" } };
		ExtentColor colorPass = ExtentColor.GREEN;
		ExtentColor colorFail = ExtentColor.RED;
		// getTest().get().log((hu.totalPageLoadTime_decimal < 10) ? Status.PASS :
		// Status.INFO, MarkupHelper.createTable(pageData));
		getTest().get().log(Status.INFO, MarkupHelper.createTable(pageData));
		SQLConnectionHelper.storePMASintoDB(pageName, hu.totalPageLoadTime_decimal, "5", hu.totalPageSize_decimal,
				hu.totalPageLoadTime_decimal < 10 ? "PASS" : "FAIL", outputPath);

	}

	public static void isjQueryLoaded(WebDriver driver) {
		System.out.println("Waiting for ready state complete");
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				JavascriptExecutor js = (JavascriptExecutor) d;
				String readyState = js.executeScript("return document.readyState").toString();
				System.out.println("Ready State: " + readyState);
				return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");
			}
		});
	}
}
