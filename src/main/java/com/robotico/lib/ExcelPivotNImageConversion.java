package com.robotico.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataConsolidateFunction;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFPivotTable;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCacheField;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCacheFields;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDataField;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDataFields;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPivotField;

import com.aspose.cells.Cells;
import com.aspose.cells.ImageOrPrintOptions;
import com.aspose.cells.ImageType;
import com.aspose.cells.PivotTable;
import com.aspose.cells.SheetRender;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.robotico.listener.ExtentTestNGITestListener;

public class ExcelPivotNImageConversion {

	/**
	 * 
	 * @param sourceFilePath
	 * @param PivotSheetName
	 * @param pivotColumns        a HASH MAP WHICH HOLDS KEY AS COLUMN NAME , AND
	 *                            VALUE AS COLUMN NUMBER
	 * @param destinationFilePath
	 */
	public static void createPivotTable(String sourceFilePath, String RowLabel, ArrayList<String> pivotColumns,
			String destinationFilePath, String Formula, String formulaFieldName,int firstrownumber,int lastrownumber) {
		try {
			/* Read the input file that contains the data to pivot */
			FileInputStream input_document = new FileInputStream(new File(sourceFilePath));
			/* Create a POI XSSFWorkbook Object from the input file */
			XSSFWorkbook my_xlsx_workbook = new XSSFWorkbook(input_document);
			/* Read Data to be Pivoted - we have only one worksheet */
			XSSFSheet sheet = my_xlsx_workbook.getSheetAt(0);
			/* Get the reference for Pivot Data */

			int firstRow = sheet.getFirstRowNum();
			int lastRow = sheet.getLastRowNum();
			int firstCol = sheet.getRow(0).getFirstCellNum();
			int lastCol = sheet.getRow(0).getLastCellNum();

			CellReference topLeft = new CellReference(firstRow+firstrownumber, firstCol);
			CellReference botRight = new CellReference(lastRow+lastrownumber, lastCol - 1);

			AreaReference a = new AreaReference(topLeft, botRight, null);
			/* Find out where the Pivot Table needs to be placed */
			CellReference pos = new CellReference("A1");
			/* Create Pivot Table */
			XSSFSheet pivot_sheet = my_xlsx_workbook.createSheet(RowLabel);
			XSSFPivotTable pivotTable = pivot_sheet.createPivotTable(a, pos, sheet);
			/* Add filters */
			// pivotTable.addReportFilter(17);
			pivotTable.addRowLabel(getCellIndex(sheet, RowLabel));
			for (int i = 0; i < pivotColumns.size(); i++) {
				pivotTable.addColumnLabel(DataConsolidateFunction.SUM, getCellIndex(sheet, pivotColumns.get(i)),
						pivotColumns.get(i));
			}

			if (!Formula.equalsIgnoreCase("")) {
				addFormulaToCache(pivotTable, Formula, formulaFieldName);
				addPivotFieldForNewColumn(pivotTable);
				addFormulaColumn(pivotTable, formulaFieldName);
			}

			FileOutputStream output_file = new FileOutputStream(new File(destinationFilePath));
			/* Write Pivot Table to File */
			my_xlsx_workbook.write(output_file);
			input_document.close();
			System.out.println("Pivot Table creation successful");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static synchronized void convertPivotTableToImage(String ExcelFilePathName, String ImageName,
			String... sheetName) {
		try {
			Workbook workbook = new Workbook(ExcelFilePathName);
			Worksheet worksheet;
			if (sheetName.length > 0)
				worksheet = workbook.getWorksheets().get(sheetName[0]);
			else
				worksheet = workbook.getWorksheets().get(0);

			PivotTable table = worksheet.getPivotTables().get(0);
			table.refreshData();
			table.calculateData();

			worksheet.getPageSetup().setLeftMargin(0);
			worksheet.getPageSetup().setRightMargin(0);
			worksheet.getPageSetup().setTopMargin(1);
			worksheet.getPageSetup().setBottomMargin(0);

			ImageOrPrintOptions options = new ImageOrPrintOptions();
			options.setOnePagePerSheet(true);
			options.setImageType(ImageType.PNG);

			// Take the image of your worksheet
			SheetRender sr = new SheetRender(worksheet, options);
			sr.toImage(0, ExtentTestNGITestListener.passLocation + MainUtil.file_separator + ImageName + ".png");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String readLastcellData4mPivotTable(String ExcelFilePathName, String... sheetName) {
		String lastCellData="";
		try {
			Workbook workbook = new Workbook(ExcelFilePathName);
			Worksheet worksheet;
			if (sheetName.length > 0)
				worksheet = workbook.getWorksheets().get(sheetName[0]);
			else
				worksheet = workbook.getWorksheets().get(0);

			PivotTable table = worksheet.getPivotTables().get(0);
			table.refreshData();
			table.calculateData();

			Cells cells = worksheet.getCells();
			lastCellData = cells.getLastCell().getStringValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lastCellData;
	}

	private static void addFormulaToCache(XSSFPivotTable pivotTable, String formula, String fieldName) {
		CTCacheFields ctCacheFields = pivotTable.getPivotCacheDefinition().getCTPivotCacheDefinition().getCacheFields();
		CTCacheField ctCacheField = ctCacheFields.addNewCacheField();
		ctCacheField.setName(fieldName); // Any field name
		ctCacheField.setFormula(formula);
		ctCacheField.setDatabaseField(false);
		ctCacheField.setNumFmtId(0);
		ctCacheFields.setCount(ctCacheFields.sizeOfCacheFieldArray()); // !!! update count of fields directly
	}

	private static void addPivotFieldForNewColumn(XSSFPivotTable pivotTable) {
		CTPivotField pivotField = pivotTable.getCTPivotTableDefinition().getPivotFields().addNewPivotField();
		pivotField.setDataField(true);
		pivotField.setDragToCol(false);
		pivotField.setDragToPage(false);
		pivotField.setDragToRow(false);
		pivotField.setShowAll(false);
		pivotField.setDefaultSubtotal(false);

	}

	private static void addFormulaColumn(XSSFPivotTable pivotTable, String headerName) {
		CTDataFields dataFields;
		if (pivotTable.getCTPivotTableDefinition().getDataFields() != null) {
			dataFields = pivotTable.getCTPivotTableDefinition().getDataFields();
		} else {
			// can be null if we have not added any column labels yet
			dataFields = pivotTable.getCTPivotTableDefinition().addNewDataFields();
		}
		CTDataField dataField = dataFields.addNewDataField();
		dataField.setName(headerName);
		// set index of cached field with formula - it is the last one!!!
		dataField.setFld(
				pivotTable.getPivotCacheDefinition().getCTPivotCacheDefinition().getCacheFields().getCount() - 1);
		dataField.setBaseItem(0);
		dataField.setBaseField(0);
	}

	public static int getCellIndex(XSSFSheet sheet, String columnName) {
		int coefficient = 0;
		Row row = sheet.getRow(0);
		int cellNum = row.getPhysicalNumberOfCells();
		for (int i = 0; i < cellNum; i++) {
			if ((row.getCell(i).toString()).equals(columnName)) {
				coefficient = i;
				break;
			}
		}
		return coefficient;
	}

}
